<%--
  Created by IntelliJ IDEA.
  User: vivek
  Date: 16/12/14
  Time: 6:59 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title> Currency adder</title>
    <script>
        function validate(){
            var valid_dollar_amt_regex = /^\$?[0-9][0-9]*[0-9]\.?[0-9]{0,2}$/i;
            var source1=document.getElementById("source1").value;
            var source2=document.getElementById("source2").value;
            var destcurr=document.getElementById("destcurrency").value;
            var sourcecurr1=document.getElementById("sourcecurrency1").value;
            var sourcecurr2=document.getElementById("sourcecurrency2").value;
            if(valid_dollar_amt_regex.test(source1) && valid_dollar_amt_regex.test(source2)){
                 if(sourcecurr1=='' || sourcecurr2=='')
                 {
                     alert('please select source currency ');
                     return false;
                 }
                 else if(destcurr==''){
                     alert('please select dest currency');
                     return false;
                 }
                else
                     return true;
            }
            else{
                alert("enter valid currency in numbers")
                return false;
            }

        }
    </script>
</head>

<body>
<g:form controller="currencyAdder" action="add">
    <table width="50%">
        <tr>
            <td>
                Enter the Source Currency 1
            </td>
            <td>
                <input id="source1" name="source1" type="text"/>
            </td>
            <td>
                <g:select name="sourcecurrency1" from="${currencyList}"  noSelection="${['':'Select One...']}"/>
            </td>
        </tr>
        <tr>
            <td>
                Enter Source Currency 2
            </td>
            <td>
                <input id="source2" name="source2" type="text"/>
            </td>
            <td>
                <g:select name="sourcecurrency2" from="${currencyList}"  noSelection="${['':'Select One...']}"/>
            </td>
        </tr>
        <tr>
            <td >
                Enter the final currency
            </td>
            <td colspan="2">
                <g:select id="destcurrency" name="destcurrency" from="${currencyList}"  noSelection="${['':'Select One...']}"/>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <input name="submit" type="submit" value="submit" onclick="return validate()" />
            </td>
        </tr>
    </table>
</g:form>

</body>
</html>