package com.simpragma.assignment

import grails.converters.JSON
import grails.plugins.rest.client.RestBuilder
import grails.plugins.rest.client.RestResponse
import org.codehaus.groovy.grails.web.json.JSONObject

class CurrencyAdderController {

    //landing page can be used for
    def index() {

        def currencyList =["USD","INR","AUD","EUR"]

        [currencyList:currencyList]
    }

    // Below line gets wired to the service class by Dependency Injection
    // this is to prevent the controller from having any business logic and becoming bulky
    def currencyConvertorService
    /*
        Below is the action to add values in different given currencies and print the value in the given final currency
    */
    def add(){
//        println(" params ${params} ")
//        def url="https://www.google.com/finance/converter?a=S1&from=C1&to=C2"

        double sourceValue1= params.source1 as double  //value entered for 1st currency
        double sourceValue2= params.source2 as double  //value entered for 2nd currency

        //converting source values to base on 2nd source currency
        double finalSourceValue=0
        def sourceValue=currencyConvertorService.convertCurrency(sourceValue1,params.sourcecurrency1,params.sourcecurrency2)
        if(sourceValue==-1)
            render(view:'error',model:[errors:"Error comverting base currencies"])
        else if(sourceValue==-2)
            render(view:'error',model:[errors:"could not reach internet/network"])
        else
            finalSourceValue=sourceValue+sourceValue2

        //converting sum of the above values in the 2nd currency to final dest currency
        double finalDestValue=0
        def destValue=currencyConvertorService.convertCurrency(finalSourceValue,params.sourcecurrency2,params.destcurrency)
        if(destValue==-1)
            render(view:'error',model:[errors:"Error comverting to final currency"])
        else if(destValue==-2)
            render(view:'error',model:[errors:"could not reach internet/network"])
        else
            finalDestValue=destValue

        finalDestValue = finalDestValue.round(2)
        [value:finalDestValue, currency:params.destcurrency]
    }

    def error(){

    }
}
