package currencyadder

import grails.converters.JSON
import grails.plugins.rest.client.RestBuilder
import grails.plugins.rest.client.RestResponse
import grails.transaction.Transactional

@Transactional
class CurrencyConvertorService {

    def serviceMethod() {

    }

    /* Method  to get rates and convert source currency to dest currency
    *  used a webservice which is opened on rest call (3r party rest resource)
    *  require internet the api gives a json object with rate,to,from
    *   we use the rate and multiply source1 to get the result
    *   */
    def convertCurrency(double sourceValue1,String sourceCurrency1,String sourceCurrency2){

        def url="http://rate-exchange.appspot.com/currency?from=C1&to=C2"
        def urlForConversion = url.replace("C1",sourceCurrency1).replace("C2",sourceCurrency2)
        RestBuilder rest = new RestBuilder()
        def rate =null
        RestResponse response= null
        try{
            response=rest.get(urlForConversion){

            }
        }catch (Exception exp){
            //this is for testing git flow
            println(" Main reason for this exception might be loss of internet/network")

            println(" ${exp.getMessage()}")
            return -2
        }


//        println(" ${response.body.toString()}")
        if(response.status==200)
        {
            def json = JSON.parse("${response.body.toString()}")
//        println json
            rate=json.getAt("rate")
//        println(" ${sourceValue1} ${rate}")
            double finalSourceValue = sourceValue1*rate
            return finalSourceValue
        }else
            return -1
    }
}
